<?php

// Отправляем браузеру правильную кодировку,
header('Content-Type: text/html; charset=UTF-8');


if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('main.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['login'])) {
    print('<div style="color:red;">Заполните имя.</div><hr>');
    $errors = TRUE;
  }
  if (empty($_POST['email'])) {
    print('<div style="color:red;">Заполните email.</div><hr>');
    $errors = TRUE;
  }
  if (empty($_POST['date'])) {
    print('<div style="color:red;">Заполните дату рождения.</div><hr>');
    $errors = TRUE;
  }
  if (empty($_POST['sex'])) {
    print('<div style="color:red;">Заполните пол.</div><hr>');
    $errors = TRUE;
  }
  if (empty($_POST['amount'])) {
    print('<div style="color:red;">Заполните количество конечностей.</div><hr>');
    $errors = TRUE;
  }
  if (empty($_POST['superpowers'])) {
    print('<div style="color:red;">Заполните сверхспособности.</div><hr>');
    $errors = TRUE;
  }
  if (empty($_POST['biography'])) {
    print('<div style="color:red;">Расскажите о себе.</div><hr>');
    $errors = TRUE;
  }
  if (empty($_POST['contract'])) {
    print('<div style="color:red;">Требуется согласиться с контрактром.</div><hr>');
    $errors = TRUE;
  }

  if ($errors) {
    // При наличии ошибок завершаем работу скрипта.
    exit();
  }
   else {
    print('<div style="color:green;">Все поля введены корректно</div><hr>');
  }

// параметры подключения

$user='u24321';
$password='36245632';
$db_name='u24321';
$db_host='localhost';
try{
//подключение к базе данных
  $db=new PDO('mysql:host='.$db_host.';dbname='.$db_name, $user, $password,
    array(PDO::ATTR_PERSISTENT => true));
//устанавливаем кодировку
  $db->exec('set names utf8');
// Подготавливаем SQL-запрос
  $query = $db->prepare("INSERT INTO application (login,email,date,sex,amount,superpowers,biography,contract)
values (:login,:email,:date,:sex,:amount,:superpowers,:biography,:contract)");
//выполняем запрос с данными
$query->bindParam(':login', $_POST['login']);
  $query->bindParam(':email', $_POST['email']);
  $query->bindParam(':date', $_POST['date']);
  $query->bindParam(':sex', $_POST['sex']);
  $query->bindParam(':amount', $_POST['amount']);
  $query->bindParam(':superpowers', $_POST['superpowers']);
  $query->bindParam(':biography', $_POST['biography']);
  $query->bindParam(':contract', $_POST['contract']);
  //PDOStatement::bindParam — Привязывает параметр запроса к переменной
  $query->execute();

}catch(PDOException $e){
// Если есть ошибка соединения или выполнения запроса, выводим её
  print ('Ошибка: '. $e->getMessage().'<br/>');
  exit();
}
header('Location: ?save=1');
